﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StronaStudent.aspx.cs" Inherits="StronaStudent" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button ID="dane" runat="server" Text="Wyświetl dane" OnClick="dane_Click"  />
        <asp:Button ID="oceny" runat="server" Text="Wyświetl oceny" OnClick="oceny_Click" />
    </div>
        <div>
            <asp:GridView ID="GridView1" runat="server" Visible="False" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical">
                <AlternatingRowStyle BackColor="#CCCCCC" />
                <Columns>
                    <asp:BoundField DataField="nr_indeksu" HeaderText="Numer indeksu" />
                    <asp:BoundField DataField="imie" HeaderText="Imię" />
                    <asp:BoundField DataField="nazwisko" HeaderText="Nazwisko" />
                    <asp:BoundField DataField="adres" HeaderText="Adres" />
                    <asp:BoundField DataField="data_urodzenia" HeaderText="Data urodzenia" />
                    <asp:BoundField DataField="pesel" HeaderText="Pesel" />
                    <asp:BoundField DataField="data_rozpoczecia" HeaderText="Data rozpoczęcia studiów" />
                    <asp:BoundField DataField="aktualny_sem" HeaderText="Aktualny semestr" />
                    <asp:BoundField DataField="tryb_studiow" HeaderText="Tryb studiów" />
                    <asp:BoundField DataField="kierunek" HeaderText="Kierunek" />
                    <asp:BoundField DataField="mail" HeaderText="Adres e-mail" />
                </Columns>
                <FooterStyle BackColor="#CCCCCC" />
                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#808080" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#383838" />
            </asp:GridView>
        </div>
    </form>
</body>
</html>
