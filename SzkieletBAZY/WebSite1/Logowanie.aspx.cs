﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Logowanie : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        //Sess
        string login = TextBox1.Text;
        string haslo = this.haslo.Text;
        if (login != "" && haslo != "")
        {
            if (login == "admin" && haslo == "admin")
            {
                Response.Redirect("StronaAdmin.aspx");
            }
            else if (SprawdzStudenta(login,haslo))
            {
                Session["studentLogin"] = login;
                Session["studentHaslo"] = haslo;
                Response.Redirect("StronaStudent.aspx");
            }
            else if (SprawdzWykladowce(login, haslo))
            {
                Session["wykladowcaLogin"] = login;
                Session["wykladowcaHaslo"] = haslo;
                Response.Redirect("StronaWykladowca.aspx");
            }
            else
            {
                //TODO Bledne logowanie
            }

        }
    }
    private bool SprawdzStudenta(string login, string haslo)
    {
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename='C:\Users\Łukasz\Dysk Google\studia\3 rok\Projekt isk2\SzkieletBAZY\WebSite1\App_Data\DziekanatDB.mdf';Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText = String.Format("select imie from Studenci where Studenci.nr_indeksu = '{0}' and Studenci.haslo = '{1}'", TextBox1.Text, this.haslo.Text);
        command.CommandType = CommandType.Text;
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        connection.Close();
        if (dt.Rows.Count != 0)
        {
            return true;
        }
        return false;
    }
    private bool SprawdzWykladowce(string login, string haslo)
    {
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename='C:\Users\Łukasz\Dysk Google\studia\3 rok\Projekt isk2\SzkieletBAZY\WebSite1\App_Data\DziekanatDB.mdf';Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText = String.Format("select imie from Wykladowcy where Wykladowcy.login = '{0}' and Wykladowcy.haslo = '{1}'", login, haslo);
        command.CommandType = CommandType.Text;
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        connection.Close();
        if (dt.Rows.Count != 0)
        {
            return true;
        }
        return false;
    }
}